ARG VERSION
FROM php:${VERSION}-fpm-bullseye
# This argument needs to be after the FROM in order to be defined.
ARG TARGETARCH

# Install packages
RUN apt-get update \
    && apt-get install -y \
            apt-transport-https \
            apt-utils \
            ca-certificates \
            curl \
            git \
            gnupg2 \
            openssh-client \
            patch \
            rsync \
            software-properties-common \
            unzip \
            zip

RUN mkdir -p /etc/apt/keyrings

# Add repositories
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
    && echo "deb [arch=$TARGETARCH signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [arch=$TARGETARCH signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x nodistro main" > /etc/apt/sources.list.d/nodesource.list

RUN curl -fsSL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor -o /etc/apt/keyrings/yarnpkg.gpg \
    && echo "deb [arch=$TARGETARCH signed-by=/etc/apt/keyrings/yarnpkg.gpg] https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# Install packages
RUN apt-get update \
    && apt-get install -y \
            docker-ce docker-compose-plugin \
            nodejs \
            yarn

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Remove stuff
RUN rm -rf /usr/bin/docker-containerd*
RUN rm -rf /usr/include /usr/share/man /tmp/* /root/.cache

RUN apt-get autoclean -y
RUN apt-get autoremove -y
RUN apt-get clean -y
