ARG VERSION
ARG PHP_IMAGE_VERSION

# Load node version
FROM node:${VERSION}-bullseye AS node

# PHP for database dumb
FROM registry.gitlab.com/foodsharing-dev/images/php:${PHP_IMAGE_VERSION}

# Change to root
USER root

# Copy node to PHP image
COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

# Set node module path to global modules
RUN npm root -g

# Install global modules
RUN npm install -g sql-ddl-to-json-schema
RUN npm install -g --force yarn

# 33 == www-data
# for unknown reasons Kanthaus ignores USER when running these images,
# and instead always runs as www-data.
# Run everything as that user everywhere to avoid permission headaches.
ARG UID=33
ARG GID=33

# Fix permissions
# There is 100% a better solution
RUN chown -R ${UID}:${GID} /usr/lib
RUN chown -R ${UID}:${GID} /usr/local

RUN mkdir -p /app
RUN chown -R ${UID}:${GID} /app
RUN chmod 775 /app

# set up home dir for www-data, which does not exist in the base image
RUN mkdir -p /var/www
RUN chown -R ${UID}:${GID} /var/www
RUN chmod 775 /var/www

USER ${UID}:${GID}

WORKDIR /app
